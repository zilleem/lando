<?php

// Local alias
$aliases['local'] = array(
  'root' => '/app/docroot',
  'uri'  => '[project].lndo.site',
  'path-aliases' => array(
    '%dump-dir' => '/tmp',
  ),
);
