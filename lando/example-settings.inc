<?php

/**
 * Lando specific settings.
 */
$drupal_hash_salt = 'UngqEOh6UXxNjFenILVJAsoA89SxuPBOTP5drDe3wQE';
$databases['default']['default'] = array(
  'database' => 'drupal7',
  'username' => 'drupal7',
  'password' => 'drupal7',
  'host' => 'database',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
);
$base_url = 'http://encorerehab.lndo.site';
$settings['cache'] = FALSE;
$settings['block_cache'] = FALSE;
$settings['page_compression'] = FALSE;
$settings['preprocess_css'] = FALSE;
$settings['preprocess_js'] = FALSE;
