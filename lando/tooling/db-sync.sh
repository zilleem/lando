#!/bin/bash
if test $1; then
  echo "Synchronizing database from $1..."
  drush cc drush
  drush pipe @$1 @local --dump --temp --progress -y
else
  echo "Usage: lando db-sync [env] where [env] is dev, test or prod."
fi
