# lando

Lando starter recipes

The `lando` directory has all the goodies like api keys, .pem files, Drush aliases, php/mysql configuration and tooling scripts

## Examples:
- `.lando.yml-drupal7.example` => Basic Drupal 7 site recipe
- `.lando.yml-drupal8.example` => Basic Drupal 8 site recipe
- `.lando.yml-drupal7-acquia.example` => Acquia Cloud Drupal 7 site recipe
- `.lando.yml-drupal8-acquia.example` => Acquia Cloud Drupal 8 site recipe

## Usage:
- Copy the example recipe you would like to use to your project root directory and rename it to `.lando.yml`.
- Recursively copy the `lando` directory to your project root directory, if applicable.
- Replace all `[project]` placeholder text with your project name. Example: `sed -i 's,[project],my-site,g' .lando.yml`
- Execute `lando start` and let the fun begin.

## Caveats:
- Make sure you have all the correct configuration relevant to your project configured in the `lando` directory, if necessary.
